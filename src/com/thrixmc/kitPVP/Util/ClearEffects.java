package com.thrixmc.kitPVP.Util;

import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffectType;

public class ClearEffects {
	public static void clear(Player p)
	{
	if (p.hasPotionEffect(PotionEffectType.BLINDNESS) == true)
	{
	p.removePotionEffect(PotionEffectType.BLINDNESS);
	}
	if (p.hasPotionEffect(PotionEffectType.CONFUSION) == true)
	{
	p.removePotionEffect(PotionEffectType.CONFUSION);
	}
	if (p.hasPotionEffect(PotionEffectType.DAMAGE_RESISTANCE) == true)
	{
	p.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
	}
	if (p.hasPotionEffect(PotionEffectType.FAST_DIGGING) == true)
	{
	p.removePotionEffect(PotionEffectType.FAST_DIGGING);
	}
	if (p.hasPotionEffect(PotionEffectType.FIRE_RESISTANCE) == true)
	{
	p.removePotionEffect(PotionEffectType.FIRE_RESISTANCE);
	}
	if (p.hasPotionEffect(PotionEffectType.HEAL) == true)
	{
	p.removePotionEffect(PotionEffectType.HEAL);
	}
	if (p.hasPotionEffect(PotionEffectType.HUNGER) == true)
	{
	p.removePotionEffect(PotionEffectType.HUNGER);
	}
	if (p.hasPotionEffect(PotionEffectType.INCREASE_DAMAGE) == true)
	{
	p.removePotionEffect(PotionEffectType.INCREASE_DAMAGE);
	}
	if (p.hasPotionEffect(PotionEffectType.JUMP) == true)
	{
	p.removePotionEffect(PotionEffectType.JUMP);
	}
	if (p.hasPotionEffect(PotionEffectType.POISON) == true)
	{
	p.removePotionEffect(PotionEffectType.POISON);
	}
	if (p.hasPotionEffect(PotionEffectType.REGENERATION) == true)
	{
	p.removePotionEffect(PotionEffectType.REGENERATION);
	}
	if (p.hasPotionEffect(PotionEffectType.SLOW) == true)
	{
	p.removePotionEffect(PotionEffectType.SLOW);
	}
	if(p.hasPotionEffect(PotionEffectType.SLOW_DIGGING) == true)
	{
	p.removePotionEffect(PotionEffectType.SLOW_DIGGING);
	}
	if (p.hasPotionEffect(PotionEffectType.SPEED) == true)
	    {
	        p.removePotionEffect(PotionEffectType.SPEED);
	    }
	if (p.hasPotionEffect(PotionEffectType.WATER_BREATHING) == true)
	{
	p.removePotionEffect(PotionEffectType.WATER_BREATHING);
	}
	if (p.hasPotionEffect(PotionEffectType.WEAKNESS) == true)
	{
	p.removePotionEffect(PotionEffectType.WEAKNESS);
	}
	 
	}
	 
}
