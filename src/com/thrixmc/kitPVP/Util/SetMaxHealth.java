/*
 * 
 * Copyright Jboy 2015, ALL RIGHTS RESERVED
 * Created Jul 23, 2015 at 10:08:05 AM 
 *
 */
package com.thrixmc.kitPVP.Util;

import org.bukkit.entity.Player;

import com.thrixmc.kitPVP.Chat.Message;

/**
 * @author John
 *
 */
public class SetMaxHealth {

	public static void SetMax(Player p, Boolean b) {

		p.setHealth(20);
		p.setFoodLevel(20);
		if (b == true) {
			Message.tellPlayer(p, "You have been healed!");
		}

	}

}
