package com.thrixmc.kitPVP.GUI.kit.KitGive;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.thrixmc.kitPVP.Util.ClearINV;

public class Archer {
	/*
	 * 
	 * Copyright Jboy 2015, ALL RIGHTS RESERVED
	 */
	public static void Give(Player p) {
		ClearINV.Clear(p);
		/*
		 * ARMOR
		 */
		p.getInventory().getBoots().setType(Material.CHAINMAIL_BOOTS);
		p.getInventory().getLeggings().setType(Material.CHAINMAIL_LEGGINGS);
		p.getInventory().getChestplate().setType(Material.CHAINMAIL_CHESTPLATE);
		p.getInventory().getHelmet().setType(Material.CHAINMAIL_HELMET);
		/*
		 * ITEMS
		 */
		ItemStack bow = new ItemStack(Material.BOW);
		ItemMeta bowMeta = bow.getItemMeta();
		bowMeta.addEnchant(Enchantment.ARROW_INFINITE, 2, true);
		bow.setItemMeta(bowMeta);

		p.getInventory().setItem(1, bow);
		p.getInventory().addItem(new ItemStack(Material.ARROW));
		/*
		 * SOUP
		 */
		p.getInventory().addItem(new ItemStack(Material.MUSHROOM_SOUP));
		p.getInventory().addItem(new ItemStack(Material.MUSHROOM_SOUP));
		p.getInventory().addItem(new ItemStack(Material.MUSHROOM_SOUP));
		p.getInventory().addItem(new ItemStack(Material.MUSHROOM_SOUP));
		p.getInventory().addItem(new ItemStack(Material.MUSHROOM_SOUP));
		p.getInventory().addItem(new ItemStack(Material.MUSHROOM_SOUP));
		p.getInventory().addItem(new ItemStack(Material.MUSHROOM_SOUP));
		p.getInventory().addItem(new ItemStack(Material.MUSHROOM_SOUP));
		p.getInventory().addItem(new ItemStack(Material.MUSHROOM_SOUP));
		p.getInventory().addItem(new ItemStack(Material.MUSHROOM_SOUP));

	}

	@SuppressWarnings("unused")
	@Deprecated
	private static void GiveArmor(Player p) {
	}

	@SuppressWarnings("unused")
	@Deprecated
	private static void GiveItems(Player p) {
	}
}
