package com.thrixmc.kitPVP.GUI;

import org.bukkit.Bukkit;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.thrixmc.kitPVP.Core;
import com.thrixmc.kitPVP.GUI.KitIcon.Archer;
import com.thrixmc.kitPVP.GUI.KitIcon.Assassin;
import com.thrixmc.kitPVP.GUI.KitIcon.Pvp;
import com.thrixmc.kitPVP.GUI.KitIcon.TntFighter;
import com.thrixmc.kitPVP.GUI.KitIcon.juggern;
import com.thrixmc.kitPVP.GUI.KitIcon.ninja;
import com.thrixmc.kitPVP.GUI.KitIcon.tank;

public class Gui {

	Core plugin;

	private static ItemStack kitOne;

	private static ItemStack KitTwo;

	private static ItemStack KitThree;

	private static ItemStack KitFour;

	private static ItemStack kitFive;

	private static ItemStack assassinkit;
	
	private static ItemStack juggernaught;

	public static Inventory Gui = Bukkit.createInventory(null, 18, "KITS");

	static {

		kitOne = Pvp.pvp("Pvp");

		KitTwo = Archer.archer("Archer");

		KitThree = ninja.Ninja("Ninja");

		KitFour = tank.Tank("Tank");

		kitFive = TntFighter.tntFighter("TnT Fighter");
		
		juggernaught = juggern.JUggerNaught("Jugger Naught");

		assassinkit = Assassin.assassin("Assassin Kit");

		Gui.setItem(0, kitOne);

		Gui.setItem(1, KitTwo);

		Gui.setItem(2, KitThree);

		Gui.setItem(3, KitFour);

		Gui.setItem(4, kitFive);

		Gui.setItem(5, assassinkit);
		
		Gui.setItem(6, juggernaught);

	}

}
