/*
 * 
 * Copyright Jboy 2015, ALL RIGHTS RESERVED
 * Created Jul 24, 2015 at 3:47:18 PM 
 *
 */
package com.thrixmc.kitPVP.GUI.KitIcon;

import java.util.Arrays;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * @author John
 *
 */
public class tank {
	public static ItemStack Tank(String name) {
		ItemStack i = new ItemStack(new ItemStack(Material.DIAMOND_SWORD, 1));
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(name);
		im.setLore(Arrays.asList("Tank through them all!"));
		im.addEnchant(Enchantment.DAMAGE_ALL, 2, true);
		i.setItemMeta(im);
		return i;
	}
}
