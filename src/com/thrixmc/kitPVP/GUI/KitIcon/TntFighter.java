package com.thrixmc.kitPVP.GUI.KitIcon;

import java.util.Arrays;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class TntFighter {

	public static ItemStack tntFighter(String name) {
		ItemStack i = new ItemStack(new ItemStack(Material.TNT, 1));
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(name);
		im.setLore(Arrays.asList("Blow everyone up with TNT!!"));
		i.setItemMeta(im);
		return i;
	}
}
