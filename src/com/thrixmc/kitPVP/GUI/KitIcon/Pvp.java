package com.thrixmc.kitPVP.GUI.KitIcon;

import java.util.Arrays;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Pvp {
	public static ItemStack pvp(String name) {
		ItemStack i = new ItemStack(new ItemStack(Material.IRON_SWORD, 1));
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(name);
		im.setLore(Arrays.asList("Standerd PVP"));
		i.setItemMeta(im);
		return i;
	}
}
