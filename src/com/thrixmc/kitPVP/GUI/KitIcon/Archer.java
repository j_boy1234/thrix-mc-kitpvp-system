package com.thrixmc.kitPVP.GUI.KitIcon;

import java.util.Arrays;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Archer {
	public static ItemStack archer(String name) {
		ItemStack i = new ItemStack(new ItemStack(Material.BOW, 1));
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(name);
		im.setLore(Arrays.asList("Fire the arrows away!"));
		i.setItemMeta(im);

		return i;
	}
}
