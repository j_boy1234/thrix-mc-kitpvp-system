package com.thrixmc.kitPVP.GUI.KitIcon;

import java.util.Arrays;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Assassin {

	public static ItemStack assassin(String name) {
		ItemStack i = new ItemStack(new ItemStack(Material.REDSTONE, 1));
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(name);
		im.setLore(Arrays.asList("Speedy Kill."));
		i.setItemMeta(im);
		return i;
	}
}
