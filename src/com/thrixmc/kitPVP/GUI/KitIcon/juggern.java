package com.thrixmc.kitPVP.GUI.KitIcon;

import java.util.Arrays;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class juggern {

	public static ItemStack JUggerNaught(String name) {
		ItemStack i = new ItemStack(new ItemStack(Material.DIAMOND_CHESTPLATE, 1));
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(name);
		im.setLore(Arrays.asList("Strong, but slow!"));
		im.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 2, true);
		i.setItemMeta(im);
		return i;
	}
	
}
