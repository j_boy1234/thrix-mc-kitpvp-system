package com.thrixmc.kitPVP.GUI.KitIcon;

import java.util.Arrays;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ninja {
	public static ItemStack Ninja(String name) {
		ItemStack i = new ItemStack(new ItemStack(Material.SUGAR, 1));
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(name);
		im.setLore(Arrays.asList("Fast, But Deadly"));
		i.setItemMeta(im);
		return i;
	}
}
