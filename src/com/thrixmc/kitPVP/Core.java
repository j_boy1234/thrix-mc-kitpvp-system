package com.thrixmc.kitPVP;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;

import com.thrixmc.kitPVP.Commands.KitCommand;
import com.thrixmc.kitPVP.Events.KitClickEvent;
import com.thrixmc.kitPVP.Events.PlayerDeathE;
import com.thrixmc.kitPVP.Events.PlayerJoinE;
import com.thrixmc.kitPVP.Events.RespawnEvent;
import com.thrixmc.kitPVP.Events.SoupClickEvent;
import com.thrixmc.kitPVP.Events.KitEvents.Tnt;
import com.thrixmc.kitPVP.Util.UnLoad;
import com.thrixmc.kitPVP.Util.load;
import com.thrixmc.kitPVP.signs.signchange.ChangeSigns;
import com.thrixmc.kitPVP.signs.soup.RefillSoup;

public class Core extends JavaPlugin implements Listener {
	/*
	 * 
	 * Copyright Jboy 2015, ALL RIGHTS RESERVED
	 */
	PluginManager pm = getServer().getPluginManager();
	public static String PluginName = "KitPVP";
	public static String Server = "THRIX-MC";
	public static String author = "j_boy1234";
	private static Core plugin;

	@SuppressWarnings("static-access")
	public void onEnable() {
		pm.registerEvents(new PlayerJoinE(), this);
		pm.registerEvents(new KitClickEvent(), this);
		pm.registerEvents(new SoupClickEvent(), this);
		pm.registerEvents(new RespawnEvent(), this);
		pm.registerEvents(new PlayerDeathE(), this);
		pm.registerEvents(new ChangeSigns(), this);
		pm.registerEvents(new RefillSoup(), this);
		pm.registerEvents(new Tnt(), this);
		pm.registerEvents(this, this);
		getCommand("openkits").setExecutor(new KitCommand());
		this.plugin = this;
		load.loadPlugin();
	}

	@SuppressWarnings("static-access")
	public void onDisable() {
		UnLoad.DeLoad();
		this.plugin = null;
		//Unload
		/*
		 * 
		 */
	}

	public static Plugin getPlugin() {
		return plugin;
	}

	@SuppressWarnings("deprecation")
	public boolean onCommand(CommandSender sender, Command cmd,
			String CommandLabel, String[] args) {

		if ((cmd.getName().equalsIgnoreCase("8903241089791234589074326"))) {
			if (sender.hasPermission("core.fjldksfj")) {
				Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
						"s2848jdufhdif4 " + args[0] + " iron");
				Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
						"83409213848item " + args[0]);
				Player p = (Player) Bukkit.getPlayer(args[0]);
				// p.openInventory(Gui.Gui);
				BukkitScheduler scheduler = Bukkit.getServer().getScheduler();
				scheduler.scheduleAsyncDelayedTask(this, new Runnable() {
					@Override
					public void run() {

						Bukkit.dispatchCommand(p.getPlayer(), "openkits");

					}
				}, 20L);

				// Bukkit.dispatchCommand(p, "openkits");
			}
		}
		if (cmd.getName().equalsIgnoreCase("83409213848item")) {
			if (sender.hasPermission("core.djfouisdeh")) {
				Player p = (Player) Bukkit.getPlayer(args[0]);
				p.getPlayer().getInventory()
						.setItem(1, new ItemStack(Material.IRON_SWORD));
				p.getPlayer().getInventory()
						.addItem(new ItemStack(Material.MUSHROOM_SOUP));
				p.getPlayer().getInventory()
						.addItem(new ItemStack(Material.MUSHROOM_SOUP));
				p.getPlayer().getInventory()
						.addItem(new ItemStack(Material.MUSHROOM_SOUP));
				p.getPlayer().getInventory()
						.addItem(new ItemStack(Material.MUSHROOM_SOUP));
				p.getPlayer().getInventory()
						.addItem(new ItemStack(Material.MUSHROOM_SOUP));
				p.getPlayer().getInventory()
						.addItem(new ItemStack(Material.MUSHROOM_SOUP));
				p.getPlayer().getInventory()
						.addItem(new ItemStack(Material.MUSHROOM_SOUP));
				p.getPlayer().getInventory()
						.addItem(new ItemStack(Material.MUSHROOM_SOUP));
				p.getPlayer().getInventory()
						.addItem(new ItemStack(Material.MUSHROOM_SOUP));
				p.getPlayer().getInventory()
						.addItem(new ItemStack(Material.MUSHROOM_SOUP));
			}
		}

		if ((cmd.getName().equalsIgnoreCase("s2848jdufhdif4"))) {
			if (!sender.hasPermission("thrix.admin")) {
				return false;
			}
			Player receiver = getServer().getPlayer(args[0]);
			if (receiver == null) {
				return false;
			}
			if ((args[1].equalsIgnoreCase("leather"))
					|| (args[1].equalsIgnoreCase("l"))) {
				giveArmor(receiver, 298, 299, 300, 301, "leather");
			} else if ((args[1].equalsIgnoreCase("chainmail"))
					|| (args[1].equalsIgnoreCase("c"))) {
				giveArmor(receiver, 302, 303, 304, 305, "chainmail");
			} else if ((args[1].equalsIgnoreCase("gold"))
					|| (args[1].equalsIgnoreCase("g"))) {
				giveArmor(receiver, 314, 315, 316, 317, "gold");
			} else if ((args[1].equalsIgnoreCase("iron"))
					|| (args[1].equalsIgnoreCase("i"))) {
				giveArmor(receiver, 306, 307, 308, 309, "iron");
			} else if ((args[1].equalsIgnoreCase("diamond"))
					|| (args[1].equalsIgnoreCase("d"))) {
				giveArmor(receiver, 310, 311, 312, 313, "diamond");
			} else {
			}

		}
		return true;
	}

	@SuppressWarnings("deprecation")
	public void giveArmor(Player player, int helmet, int chest, int leggings,
			int boots, String type) {
		player.getInventory().setHelmet(new ItemStack(helmet, 1));
		player.getInventory().setChestplate(new ItemStack(chest, 1));
		player.getInventory().setLeggings(new ItemStack(leggings, 1));
		player.getInventory().setBoots(new ItemStack(boots, 1));

	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onRespawn(PlayerRespawnEvent e) {
		BukkitScheduler scheduler = Bukkit.getServer().getScheduler();
		scheduler.scheduleAsyncDelayedTask(plugin, new Runnable() {
			@Override
			public void run() {
				Bukkit.dispatchCommand(e.getPlayer(), "openkits");
			}
		}, 20L);

	}
}
