/*
 * 
 * Copyright Jboy 2015, ALL RIGHTS RESERVED
 * Created Jul 23, 2015 at 1:18:58 PM 
 *
 */
package com.thrixmc.kitPVP.signs.soup;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import com.thrixmc.kitPVP.Core;
import com.thrixmc.kitPVP.Util.GiveSoup;
/**
 * @author John
 *
 */
public class RefillSoup implements Listener {

	public static Core plugin;

	@EventHandler
	public void signInteract(PlayerInteractEvent e) {
		if (e.getAction() == Action.RIGHT_CLICK_BLOCK
				|| e.getAction() == Action.LEFT_CLICK_BLOCK) {
			Block block = e.getClickedBlock();
			if (block.getType() == Material.SIGN
					|| block.getType() == Material.SIGN_POST
					|| block.getType() == Material.WALL_SIGN) {
				Sign sign = (Sign) e.getClickedBlock().getState();
				if (sign.getLine(0).contains("Refill Soup")) {
					GiveSoup.give(e.getPlayer());
				}
			}
		}
	}

}
