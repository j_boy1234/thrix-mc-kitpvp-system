package com.thrixmc.kitPVP.Chat;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.thrixmc.kitPVP.Core;

public class Message {

	public static void ConsoleSend(String s) {
		Bukkit.getLogger().info(
				ChatColor.GRAY + "" + ChatColor.BOLD + Core.Server
						+ " CONSOLESEND > " + ChatColor.RESET + s);
	}

	public static void tellPlayer(Player p, String s) {
		p.sendMessage(ChatColor.GRAY + Core.Server  + " ► " + s);
	}

	public static void broadcast(String s) {
		Bukkit.getServer().broadcastMessage(
				ChatColor.GRAY + Core.Server + " INFO ► "
						+ ChatColor.RESET + s);
	}

	public static void NoPerms(Player p) {
		p.sendMessage(ChatColor.RED
				+ Core.Server
				+ " Permissions ► You do not the the required permission to do this action!");
	}
	
	public static void antiStaffAbuse(Player p, String s){
		p.sendMessage(ChatColor.RED + "Anti Admin Abuse System - Sorry! You may not restock your kit!");
	}

}
