/*
 * 
 * Copyright Jboy 2015, ALL RIGHTS RESERVED
 * Created Jul 23, 2015 at 9:21:51 AM 
 *
 */
package com.thrixmc.kitPVP.Events;

import org.bukkit.Material;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import com.thrixmc.kitPVP.Core;

/**
 * @author John
 *
 */
public class SoupClickEvent implements Listener {

	public static Core plugin;

	@EventHandler
	public void OnPlayerSoup(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		Damageable d = (Damageable) player;
		if (d.getHealth() == 20) {
		} else {
			int soup = +7;
			if ((event.getAction() == Action.RIGHT_CLICK_AIR || event
					.getAction() == Action.RIGHT_CLICK_BLOCK)
					&& player.getItemInHand().getType() == Material.MUSHROOM_SOUP) {
				player.setHealth(d.getHealth() + soup > d.getMaxHealth() ? d
						.getMaxHealth() : d.getHealth() + soup);
				event.getPlayer().getItemInHand().setType(Material.BOWL);
			}

		}
	}

}
