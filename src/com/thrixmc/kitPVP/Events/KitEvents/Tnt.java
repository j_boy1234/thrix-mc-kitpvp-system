/*
 * 
 * Copyright Jboy 2015, ALL RIGHTS RESERVED
 * Created Jul 24, 2015 at 7:35:59 PM 
 *
 */
package com.thrixmc.kitPVP.Events.KitEvents;

import java.util.Random;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.util.Vector;

import com.thrixmc.kitPVP.Core;
import com.thrixmc.kitPVP.Chat.Message;

/**
 * @author John
 *
 */
public class Tnt implements Listener {

	public static Core plugin;

	@EventHandler
	public void onPlayerInteract1(PlayerInteractEvent eventy) {
		Player player = (Player) eventy.getPlayer();
		if (eventy.getPlayer().getItemInHand().getType() == Material.TNT) {
			if (eventy.getAction().equals(Action.LEFT_CLICK_AIR)
					|| eventy.getAction().equals(Action.LEFT_CLICK_BLOCK)) {
				if (eventy.getPlayer().getWorld().getName() != "arena") {
					final Vector direction = eventy.getPlayer()
							.getEyeLocation().getDirection().multiply(1);
					Location location = eventy.getPlayer().getLocation();
					TNTPrimed tnt = (TNTPrimed) location.getWorld().spawn(
							eventy.getPlayer()
									.getEyeLocation()
									.add(direction.getX(), direction.getY(),
											direction.getZ()), TNTPrimed.class);
					tnt.setVelocity(direction);
					Random rand = new Random();
					int randomNumber = rand.nextInt(8);

					player.getWorld().playSound(location,
							Sound.FIREWORK_LAUNCH, 10, randomNumber);

					if (player.getInventory().getItemInHand().getAmount() <= 1) {
						player.getInventory().setItemInHand(null);
					} else {
						player.getInventory()
								.getItemInHand()
								.setAmount(
										player.getInventory().getItemInHand()
												.getAmount() - 1);
					}
				} else {
					Message.tellPlayer(eventy.getPlayer(), "Not in spawn!");
				}
			}
		}
	}

}
