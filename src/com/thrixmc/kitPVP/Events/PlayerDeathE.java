package com.thrixmc.kitPVP.Events;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import com.thrixmc.kitPVP.Core;
import com.thrixmc.kitPVP.Chat.Message;

public class PlayerDeathE implements Listener {

	public static Core plugin;

	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent e) {
		if (e.getEntity() instanceof Player) {
			e.getDrops().clear();
			Player p = (Player)e.getEntity();
			KitClickEvent.hasKitList.remove(p);
			String killed = e.getEntity().getName();
			String killer = e.getEntity().getKiller().getName();
			Message.tellPlayer(e.getEntity(), "You were killed by " + killer);
			Message.tellPlayer(e.getEntity().getKiller(), "You killed "
					+ killed + " and got 50 coins!");
			Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
					"eco give " + killer + " 50");
		}
	}
}
