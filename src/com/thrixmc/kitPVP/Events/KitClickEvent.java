package com.thrixmc.kitPVP.Events;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.thrixmc.kitPVP.Core;
import com.thrixmc.kitPVP.Chat.Message;
import com.thrixmc.kitPVP.GUI.Gui;
import com.thrixmc.kitPVP.Util.ClearINV;
import com.thrixmc.kitPVP.Util.GiveSoup;

public class KitClickEvent implements Listener {
	/*
	 * 
	 * Copyright Jboy 2015, ALL RIGHTS RESERVED
	 */
	public static Core plugin;

	public static ArrayList<Player> hasKitList = new ArrayList<Player>();

	@EventHandler
	public void onInventoryClick(InventoryClickEvent e) {

		/*
		 * Inventory Checks:
		 */

		if (e.getWhoClicked() instanceof Player) {

			if (!e.getInventory().getName().equalsIgnoreCase(Gui.Gui.getName())) {
				return;
			}

			if (e.getCurrentItem().getItemMeta() == null) {
				return;
			}

			/*
			 * Now Item Checks
			 */
			Player p = (Player) e.getWhoClicked();
			if (hasKitList.contains(p)) {

				Message.tellPlayer(p,
						"Sorry! You can only use one kit per life!");
				e.setCancelled(true);
				p.closeInventory();

				if (p.isOp()) {
					Message.antiStaffAbuse(p, "");
				}

			} else {

				if (e.getCurrentItem().getItemMeta().getDisplayName()
						.equals("Pvp")) {

					if (p != null) {

						Message.tellPlayer(p, "You selected the PVP Kit!");
						hasKitList.add(p);
						// Pvp.Give(p);
						ClearINV.Clear(p);
						/*
						 * GIVE ARMOR
						 */
						Bukkit.getServer().dispatchCommand(
								Bukkit.getConsoleSender(),
								"s2848jdufhdif4 " + p.getName() + " iron");
						/*
						 * GIVE ITEMS
						 */
						p.getPlayer().getInventory()
								.setItem(1, new ItemStack(Material.IRON_SWORD));

						/*
						 * GIVE SOUP
						 */
						GiveSoup.give(p);
						e.setCancelled(true);
						p.getPlayer().closeInventory();

					}
				}
				if (e.getCurrentItem().getItemMeta().getDisplayName()
						.equals("Archer")) {

					if (p.hasPermission(Core.Server + ".kits.archer")) {
						ClearINV.Clear(p);
						/*
						 * GIVE ARMOR
						 */
						Bukkit.getServer().dispatchCommand(
								Bukkit.getConsoleSender(),
								"s2848jdufhdif4 " + p.getName() + " chainmail");
						/*
						 * GIVE ITEMS
						 */
						ItemStack bow = new ItemStack(Material.BOW);
						ItemMeta bowMeta = bow.getItemMeta();
						bowMeta.addEnchant(Enchantment.ARROW_INFINITE, 2, true);
						bow.setItemMeta(bowMeta);

						p.getInventory().setItem(1, bow);
						p.getInventory().addItem(new ItemStack(Material.ARROW));

						/*
						 * GIVE SOUP
						 */
						GiveSoup.give(p);

						Message.tellPlayer(p, "You selected the Archer Kit!");
						hasKitList.add(p);
						p.closeInventory();
						e.setCancelled(true);
					} else {
						Message.NoPerms(p);
						e.setCancelled(true);
					}

				}
				if (e.getCurrentItem().getItemMeta().getDisplayName()
						.equals("Ninja")) {

					if (p.hasPermission(Core.Server + ".kits.ninja")) {
						ClearINV.Clear(p);
						Message.tellPlayer(p, "You selected the Ninja Kit!");
						hasKitList.add(p);
						Bukkit.getServer().dispatchCommand(
								Bukkit.getConsoleSender(),
								"s2848jdufhdif4 " + p.getName() + " l");
						/*
						 * ITEMS
						 */
						ItemStack sword = new ItemStack(Material.GOLD_SWORD);
						ItemMeta swordMeta = sword.getItemMeta();
						swordMeta.addEnchant(Enchantment.DAMAGE_ALL, 2, true);
						sword.setItemMeta(swordMeta);

						p.getInventory().setItem(1, sword);
						// p.getInventory().setItem(2, new
						// ItemStack(Material.getMaterial("373:8194")));
						// p.getInventory().setItem(3, new
						// ItemStack(Material.getMaterial("373:8194")));
						/*
						 * SOUP
						 */
						GiveSoup.give(p);
						p.addPotionEffect(new PotionEffect(
								PotionEffectType.SPEED, 10000, 2));

						p.closeInventory();
						e.setCancelled(true);
					} else {
						Message.NoPerms(p);
						e.setCancelled(true);
					}

				}
				if (e.getCurrentItem().getItemMeta().getDisplayName()
						.equals("Tank")) {

					if (p.hasPermission(Core.Server + ".kits.tank")) {

						Message.tellPlayer(p, "You selected the Tank Kit!");
						hasKitList.add(p);
						/*
						 * ARMOR
						 */
						Bukkit.getServer().dispatchCommand(
								Bukkit.getConsoleSender(),
								"s2848jdufhdif4 " + p.getName() + " d");
						/*
						 * ITEMS
						 */
						p.getInventory().setItem(1,
								new ItemStack(Material.DIAMOND_SWORD));
						/*
						 * SOUP
						 */
						GiveSoup.give(p);
						p.closeInventory();
						e.setCancelled(true);
					} else {
						Message.NoPerms(p);
						e.setCancelled(true);
					}
				}

				if (e.getCurrentItem().getItemMeta().getDisplayName()
						.equals("TnT Fighter")) {
					if (p.hasPermission(Core.Server + ".kits.tntfighter")) {
						Message.tellPlayer(p, "You selected the TNT FIGHTER!");
						hasKitList.add(p);
						ClearINV.Clear(p);
						Bukkit.getServer().dispatchCommand(
								Bukkit.getConsoleSender(),
								"s2848jdufhdif4 " + p.getName() + " l");
						p.getInventory().addItem(
								new ItemStack(Material.TNT, 20));
						p.getInventory().addItem(
								new ItemStack(Material.STONE_SWORD, 1));
						GiveSoup.give(p);
						p.closeInventory();
						e.setCancelled(true);
					} else {
						Message.NoPerms(p);
						e.setCancelled(true);
					}

				}

				if (e.getCurrentItem().getItemMeta().getDisplayName()
						.equals("Assassin Kit")) {
					if (p.hasPermission(Core.Server + ".kits.assassin")) {
						Message.tellPlayer(p, "You selected the Assassin Kit!");
						hasKitList.add(p);
						ClearINV.Clear(p);
						ItemStack Assassinsword = new ItemStack(
								Material.GOLD_SWORD);
						ItemMeta AssassinMeta = Assassinsword.getItemMeta();
						AssassinMeta
								.addEnchant(Enchantment.DAMAGE_ALL, 3, true);
						Assassinsword.setItemMeta(AssassinMeta);
						p.getInventory().addItem(Assassinsword);
						p.addPotionEffect(new PotionEffect(
								PotionEffectType.SPEED, 180000, 3));
						GiveSoup.give(p);
						p.closeInventory();
						e.setCancelled(true);
					} else {
						Message.NoPerms(p);
						e.setCancelled(true);
					}
				}

				if (e.getCurrentItem().getItemMeta().getDisplayName()
						.equals("Jugger Naught")) {
					if (p.hasPermission(Core.Server + ".kits.juggernaught")) {
						Message.tellPlayer(p, "You selected Jugger Naught Kit!");
						hasKitList.add(p);
						ClearINV.Clear(p);
						ItemStack Assassinsword = new ItemStack(
								Material.DIAMOND_SWORD);
						ItemMeta AssassinMeta = Assassinsword.getItemMeta();
						AssassinMeta
								.addEnchant(Enchantment.DAMAGE_ALL, 4, true);
						Assassinsword.setItemMeta(AssassinMeta);
						p.getInventory().addItem(Assassinsword);
						Bukkit.getServer().dispatchCommand(
								Bukkit.getConsoleSender(),
								"s2848jdufhdif4 " + p.getName() + " d");
						p.addPotionEffect(new PotionEffect(
								PotionEffectType.SLOW, 180000, 3));
						GiveSoup.give(p);
						p.closeInventory();
						e.setCancelled(true);

					} else {
						Message.NoPerms(p);
						e.setCancelled(true);
					}

				}

			}
		}
	}

}
