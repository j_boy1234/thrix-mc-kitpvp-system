package com.thrixmc.kitPVP.Events;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;

import com.thrixmc.kitPVP.Core;
import com.thrixmc.kitPVP.Chat.Message;
import com.thrixmc.kitPVP.Util.ClearINV;
import com.thrixmc.kitPVP.Util.SetMaxHealth;

public class PlayerJoinE implements Listener {

	public static Core plugin;

	@EventHandler(priority = EventPriority.LOWEST)
	public void onPlayerJoin(PlayerJoinEvent e) {
		/*
		 * 
		 * Copyright Jboy 2015, ALL RIGHTS RESERVED
		 */

		Player p = (Player) e.getPlayer();

		if (!p.isOp()) {

			p.teleport(Bukkit.getServer().getWorld("arena").getSpawnLocation());
		}
		e.setJoinMessage(null);

		Message.broadcast(e.getPlayer().getName() + " Joined!");

		ClearINV.Clear(p);
		SetMaxHealth.SetMax(p, false);
		Bukkit.dispatchCommand(Bukkit.getConsoleSender(),
				"8903241089791234589074326 " + e.getPlayer().getName());
		// Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new
		// Runnable() {

		// @Override
		// public void run() {
		// Bukkit.dispatchCommand(e.getPlayer(), "openkits");
		// }
		// }, 20L);

		// Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
		// "s2848jdufhdif4 " + p.getName() + " iron");
		// p.getPlayer().getInventory().setItem(1, new
		// ItemStack(Material.IRON_SWORD));
		// / p.getPlayer().getInventory().addItem(new
		// ItemStack(Material.MUSHROOM_SOUP));
		// p.getPlayer().getInventory().addItem(new
		// ItemStack(Material.MUSHROOM_SOUP));
		// p.getPlayer().getInventory().addItem(new
		// ItemStack(Material.MUSHROOM_SOUP));
		// p.getPlayer().getInventory().addItem(new
		// ItemStack(Material.MUSHROOM_SOUP));
		// p.getPlayer().getInventory().addItem(new
		// ItemStack(Material.MUSHROOM_SOUP));
		// p.getPlayer().getInventory().addItem(new
		// ItemStack(Material.MUSHROOM_SOUP));
		// p.getPlayer().getInventory().addItem(new
		// ItemStack(Material.MUSHROOM_SOUP));
		// p.getPlayer().getInventory().addItem(new
		// ItemStack(Material.MUSHROOM_SOUP));
		// p.getPlayer().getInventory().addItem(new
		// ItemStack(Material.MUSHROOM_SOUP));
		// p.getPlayer().getInventory().addItem(new
		// ItemStack(Material.MUSHROOM_SOUP));
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerLogin(PlayerLoginEvent event) {
		if (!event.getAddress().getHostAddress().equals("158.69.60.163")) {
			event.disallow(PlayerLoginEvent.Result.KICK_OTHER,
					"Yeah Right! Join through the normal ip.");
		}
	}
}
