package com.thrixmc.kitPVP.Commands;

import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.thrixmc.kitPVP.Chat.Message;
import com.thrixmc.kitPVP.GUI.Gui;

public class KitCommand implements CommandExecutor {
	/*
	 * 
	 * Copyright Jboy 2015, ALL RIGHTS RESERVED
	 */
	@Override
	public boolean onCommand(CommandSender cs, org.bukkit.command.Command cmnd,
			String string, String[] strings) {
		if (cs instanceof Player) {
			if (string.equalsIgnoreCase("openkits")) {
				Message.tellPlayer(((Player) cs).getPlayer(),
						"Now Opening The Kit Menu!");
				((Player) cs).getPlayer().openInventory(Gui.Gui);
			}
		}
		return true;
	}

}
